﻿# Thomas Cools and Wonhyuk Choi Final Project 

## Neural Machine Translation and TTS Synthesis
The neural machine translation portion uses OpenNMT, initially created by the Harvard NLP group and SYSTRAN.
We use the [PyTorch implementation](https://github.com/OpenNMT/OpenNMT-py) of it.

The neural network consists (mainly) of three LSTM layers, modified to add Attention.

The [Google TTS library](https://github.com/pndurette/gTTS) is used to help create TTS synthesis of our translation results. 

### Dataset
We used the [Opus dataset](http://opus.nlpl.eu/Wikipedia.php), which gives English and Spanish sentence pairs for the same wikipedia article.
The dataset is 600Mb's large (2,000,000 sentence pairs) and for realistic training, we have used only 10% of the corpus.

### To train: 

If you want to train your own data, add files to the project directory, and edit the code for eng/val_train/val as fit. 

Pre-process data:
`python preprocess.py -train_src project/eng_train.txt -train_tgt project/spa_train.txt 
-valid_src project/eng_val.txt -valid_tgt project/spa_val.txt -save_data project/data`

Train: 
`python3 train.py -data project/data -save_model eng_spa_model
-world_size 1 -gpu_ranks 0 -train_steps 100000 -valid_steps 10000`

`World-size` refers to the number of GPU's, and `gpu-rank` is which ones you want to use (in this case just the first one).
We trained our model for 100,000 steps with validation every 10,000 steps. 


### Tranlsation
The model is already trained, with file name eng2spa.pt and spa2eng.pt. 
Please place your desired translation text into the project directory (in place of to_translate.txt) 
For translation from english to spanish, we will use: 

`python translate.py -model eng2spa.pt -src project/to_translate.txt
-output predidction.txt -replace_unk -verbose`

The output will result in two things: mainly, mp3 files of translation for each line fed into the program, as well as a simple txt file with the translation results. 
